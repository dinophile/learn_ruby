def add(*numbers)
  numbers.inject{|sum, n| sum + n }
end

def subtract(*numbers)
  numbers.shift - numbers.inject(:+)
end

def sum(*items)
  items[0].inject(0, :+)
end

def multiply(*numbers)
  numbers.inject(:*)
end

def factorial(n)
  (1..n).inject(1, :*)
  # => Ahhhh, I forgot that 0! equals 1 not 0!! FIXED! Woot...
end
