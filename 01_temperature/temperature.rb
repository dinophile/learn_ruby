def ftoc(temp_f)
  ((temp_f - 32)*5)/9.0
end

def ctof(temp_c)
  (temp_c * 1.8) + 32
end
