def echo(word)
  "#{word}"
end

def shout(word)
  "#{word.upcase}"
end

def repeat(word,n = 2)
  "#{word} " *(n-1) + "#{word.rstrip}"
end

def start_of_word(word, n)
  "#{word[0..n-1]}"
  # => returns the first letter of a string
end

def first_word(word)
  word.split.first
  # => returns the first word in a string
end


def titleize(word)
  word.first.capitalize
  when word[].length >= 4 do |word|
  word.capitalize
  end
  # => has to capitalize all words over the length of 4, and always the first word
  # => regardless of length
end
